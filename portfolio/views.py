from __future__ import print_function
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.contrib.auth.models import User
from portfolio.forms import *
from django.core.exceptions import ObjectDoesNotExist
import copy, random

# Create your views here.
def index(request):
    photos= Photo.objects.all()
    print(len(photos))
    data_dictionary={}
    if len(photos) > 0:
        n =24
        if len(photos)<n:
            n=len(photos)
        photos=random.sample(photos, n)
        data_dictionary['photos']=photos
    context_instance=RequestContext(request)
    for p in data_dictionary['photos']:
        print(p, p.photographer.user.username);

    return render_to_response('portfolio/index.html',data_dictionary, context_instance)


def register(request):
    data_dictionary={}
    context_instance=RequestContext(request)
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        photographer_form = PhotographerForm(data=request.POST)
        data_dictionary['user_form']=user_form
        data_dictionary['photographer_form']=photographer_form
        if user_form.is_valid() and photographer_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()
            
            photographer = photographer_form.save(commit=False)
            photographer.user=user
            if 'picture' in request.FILES:
                photographer.picture = request.FILES['picture']
            photographer.save()

            return HttpResponseRedirect('/portfolio/login/')
        else:
            print('cannot register',user_form.errors,photographer_form.errors)
        return render_to_response('portfolio/register.html',data_dictionary, context_instance)
    else:
        data_dictionary['user_form']=UserForm()
        data_dictionary['photographer_form']=PhotographerForm()
        return render_to_response('portfolio/register.html',data_dictionary, context_instance)



def random_p(request, random_type):
    context_instance=RequestContext(request)
    data_dictionary={}
    print(random_type)
    if random_type=='photographer':
        ph=Photographer.objects.all()
        print(len(ph))
        if len(ph) == 0:
            return photographers(request, None)
        else:
            ph=random.sample(ph, 1)
            i =ph[0].id
            return photographers(request, i)
    else:
        ph=Photo.objects.all()
        print(len(ph))
        if len(ph) == 0:
            return photos(request, None)
        else:
            ph=random.sample(ph, 1)
            i =ph[0].id
            return photos(request, i)

def search_for(request):
    context_instance=RequestContext(request)
    data_dictionary={}
    if not len(request.GET.keys()):
        if not request.is_ajax():
            return render_to_response('portfolio/search.html',data_dictionary, context_instance)
    else:
        if not request.is_ajax():
            m,q=None,None
            if 'search_type' in request.GET:
                m = request.GET['search_type']
                if not m in set(['photo','photographer', 'all' ]):
                    return HttpResponseBadRequest('Missing param m')
            else:
                return HttpResponseBadRequest('Missing param search_type')
            if 'search_query' in request.GET:
                q=str(request.GET['search_query'])
            else:
                return HttpResponseBadRequest('Missing param search_query')
            if m == 'photo' or m == 'all':

                print('photo', m, 'called')

                photos = Photo.objects.filter(name__icontains=q)
                if len(photos):
                    print('if', len(photos), photos, photos[0])
                    data_dictionary['photos']=photos
            if m == 'photographer' or m == 'all':
                l=[u.pk for u in User.objects.filter(username__icontains=q)]
                photographers = Photographer.objects.filter(user__in=l)
                if len(photographers):
                    data_dictionary['photographers']=photographers
            data_dictionary['results_for']=q
            return render_to_response('portfolio/search.html',data_dictionary, context_instance)



def photos(request, photo_id=None):
    context_instance=RequestContext(request)
    data_dictionary={}
    if photo_id is None:
        photos= Photo.objects.all()
        if len(photos) > 0:
            data_dictionary['photos'] = photos
        return render_to_response('portfolio/photos.html',data_dictionary, context_instance)
    else:
        try:
            photo= Photo.objects.get(id=photo_id)
            data_dictionary['photo']=photo
        except ObjectDoesNotExist, e:
            print('photo {} does not exist\n\terr:{}'.format(photo_id, e))
            return render_to_response('portfolio/photos.html',data_dictionary, context_instance, status=404)
        
        return render_to_response('portfolio/photos.html',data_dictionary, context_instance)

def photographers(request, photo_id=None):
    context_instance=RequestContext(request)
    data_dictionary={}
    if photo_id is None:
        photos= Photographer.objects.all()
        if len(photos) > 0:
            data_dictionary['photographers'] = photos
        return render_to_response('portfolio/photographers.html',data_dictionary, context_instance)
    else:
        try:
            photo= Photographer.objects.get(id=photo_id)
            data_dictionary['photographer']=photo
        except ObjectDoesNotExist, e:
            print('Photographer {} does not exist\n\terr:{}'.format(photo_id, e))
            return render_to_response('portfolio/photographers.html',data_dictionary, context_instance, status=404)
        
        return render_to_response('portfolio/photographers.html',data_dictionary, context_instance)



def login_user(request):
    data_dictionary={}
    context_instance=RequestContext(request)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if not (user is None):
            login(request, user)
            return HttpResponseRedirect('/portfolio/')
        else:
            print("Cannot Login")
            return render_to_response('portfolio/login.html', data_dictionary, context_instance)
    else:
        return render_to_response('portfolio/login.html',data_dictionary, context_instance)




@login_required
def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/')




@login_required
def account(request):
    photos = Photo.objects.filter(photographer= request.user.id)
    data_dictionary={'photos': photos}
    context_instance=RequestContext(request)
    return render_to_response('portfolio/account.html', data_dictionary, context_instance)





@login_required
def account_edit(request):
    data_dictionary={}
    context_instance=RequestContext(request)
    request_user = request.user
    request_photographer = Photographer.objects.get(user=request_user)
    print(request_photographer.id, request_user.id)
    if request.method == 'POST':
        user_form = UpdateUserForm(data=request.POST, instance=request_user)
        photographer_form = UpdatePhotographerForm(data=request.POST, instance=request_photographer)
        data_dictionary['user_form']=user_form
        data_dictionary['photographer_form']=photographer_form
        if user_form.is_valid() and photographer_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()
            
            photographer = photographer_form.save(commit=False)
            photographer.user=user
            if 'picture' in request.FILES:
                photographer.picture = request.FILES['picture']
            photographer.save()
        

            print(request_photographer.id, request_user.id)

            return HttpResponseRedirect('/portfolio/account/')
        else:
            print('cannot edit',user_form.errors,photographer_form.errors)
        return render_to_response('portfolio/account_edit.html',data_dictionary, context_instance)
    else:
        data_dictionary['user_form']=UpdateUserForm(instance=request_user)
        data_dictionary['photographer_form']=UpdatePhotographerForm(instance=request_photographer)
        return render_to_response('portfolio/account_edit.html',data_dictionary, context_instance)






@login_required
def upload(request):
    data_dictionary={}
    context_instance=RequestContext(request)

    request_user = request.user
    request_photographer = Photographer.objects.get(user=request_user)

    if request.method == 'POST':
        photo_form = PhotoForm(request.POST, request.FILES)
        data_dictionary['form']=photo_form
        if photo_form.is_valid():
            photo = photo_form.save(commit=False)
            print(photo)
            photo.photographer = Photographer.objects.get(id=request_photographer.id)

            photo.save()
            return HttpResponseRedirect('/portfolio/account/')
        else:
            print('cannot upload',photo_form.errors)
        return render_to_response('portfolio/upload.html',data_dictionary, context_instance)
    else:
        data_dictionary['form']= PhotoForm()
        return render_to_response('portfolio/upload.html', data_dictionary, context_instance)





@login_required
def account_photos_edit(request, photo_id):
    photo=get_object_or_404(Photo, pk=photo_id)
    request_photo = copy.deepcopy(photo)
    request_user = request.user

    if request_photo.photographer.user.id != request.user.id:
        return HttpResponseForbidden('Forbidden')
    request_photographer = Photographer.objects.get(user=request_user)
    data_dictionary={}
    data_dictionary['photo']=request_photo

    context_instance=RequestContext(request)
    if request.method == 'POST':
        print(request_photo, request_photo.id, request_photo.name)
        photo_form = UpdatePhotoForm(request.POST, request.FILES, instance=photo)
        data_dictionary['form']=photo_form
        if photo_form.is_valid():
            photo = photo_form.save(commit=False)
            print(photo, photo.id, photo.name)
            print(request_photo, request_photo.id, request_photo.name)

            photo.photographer = Photographer.objects.get(id=request_photographer.id)

            photo.save()
            return HttpResponseRedirect('/portfolio/account/photos/'+str(photo_id))
        else:
            print('cannot upload',photo_form.errors)
        return render_to_response('portfolio/account_photos_edit.html',data_dictionary, context_instance)
    else:
        data_dictionary['form']= UpdatePhotoForm(instance=request_photo)
        return render_to_response('portfolio/account_photos_edit.html', data_dictionary, context_instance)





@login_required
def account_photos(request, photo_id=None):
    context_instance=RequestContext(request)
    data_dictionary={}
    request_user = request.user
    request_photographer = Photographer.objects.get(user=request_user)
    photo=None
    if photo_id is None:
        photos= Photo.objects.filter(photographer=request_photographer)
        if len(photos) > 0:
            data_dictionary['photos'] = photos
        return render_to_response('portfolio/account_photos.html',data_dictionary, context_instance)
    else:
        try:
            photo= Photo.objects.get(id=photo_id)
            data_dictionary['photo']=photo
        except ObjectDoesNotExist, e:
            print('photo {} does not exist\n\terr:{}'.format(photo_id, e))
            return render_to_response('portfolio/account_photos.html',data_dictionary, context_instance, status=404)
        if photo.photographer.user.id != request.user.id:
            return HttpResponseForbidden('Forbidden')
        if request.method=='POST':
            return render_to_response('portfolio/account_photos.html',data_dictionary, context_instance, status=501)
        else:
            return render_to_response('portfolio/account_photos.html',data_dictionary, context_instance)

@login_required
def account_photos_delete(request, photo_id):
    photo = get_object_or_404(Photo, pk=photo_id)
    if photo.photographer.user.id != request.user.id:
        return HttpResponseForbidden('Forbidden')
    data_dictionary={}
    context_instance=RequestContext(request)
    if request.method == 'POST':
        print('b',len(Photo.objects.all()))
        photo.photo.delete(save=True)
        photo.delete()
        print('a',len(Photo.objects.all()))

        return render_to_response('portfolio/account.html',data_dictionary, context_instance)
    else:
        data_dictionary['photo']=photo
        return render_to_response('portfolio/account_photos_delete.html',data_dictionary, context_instance)