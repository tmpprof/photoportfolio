from django import forms
from django.contrib.auth.models import User
from portfolio.models import *
from portfolio.custom_widgets import ImageNotClearableFileInput


class UserForm(forms.ModelForm):
    username = forms.CharField(help_text='user name')
    email = forms.CharField(help_text='email')
    password = forms.CharField(widget=forms.PasswordInput(), help_text='password')
    class Meta(object):
        model = User
        fields = ('username', 'email', 'password')

class PhotographerForm(forms.ModelForm):
    about = forms.CharField(help_text='Photographer description', required=False)
    picture = forms.ImageField(help_text='Photographer picture', required=False)
    class Meta(object):
        model = Photographer
        fields = ('about', 'picture')


########################

class UpdateUserForm(forms.ModelForm):
    username = forms.CharField(help_text='user name', required=False)
    email = forms.CharField(help_text='email', required=False)
    password = forms.CharField(widget=forms.PasswordInput(), help_text='password', required=False)
    class Meta(object):
        model = User
        fields = ('username', 'email', 'password')

class UpdatePhotographerForm(forms.ModelForm):
    about = forms.CharField(help_text='Photographer description', required=False)
    picture = forms.ImageField(help_text='Photographer picture', required=False)
    class Meta(object):
        model = Photographer
        fields = ('about', 'picture')


########################




class PhotoForm(forms.ModelForm):
    name = forms.CharField(help_text='photo name')
    photo = forms.ImageField(help_text='image')
    class Meta(object):
        model = Photo
        fields = ('name','photo',)

class UpdatePhotoForm(forms.ModelForm):
    name = forms.CharField(help_text='photo name', required=False)
    photo = forms.ImageField(help_text='image', required=False, widget=ImageNotClearableFileInput)

    new_or_replace = forms.ChoiceField(choices=[('new','new'), ('replace','replace')], widget=forms.RadioSelect())
    class Meta(object):
        model = Photo
        fields = ('name','photo',)

################################################################################