from __future__ import print_function
from django.db import models
from django.contrib.auth.models import User
import os
# Create your models here.
def handle_upload_to(photo, fn):
    return 'photos/{}'.format(fn)

class Tag(models.Model):
    name = models.CharField(max_length=128, unique=True)

class Photographer(models.Model):
    user = models.OneToOneField(User)
    about = models.CharField(max_length=1024, blank=True)
    picture = models.ImageField(upload_to='picture', blank=True)

class Photo(models.Model):
    name = models.CharField(max_length=256)
    photographer =  models.ForeignKey(Photographer)
    photo = models.ImageField(upload_to=handle_upload_to)
    date=models.DateTimeField(auto_now_add=True)
    def delete(self, *args, **kwargs):
        self.photo.delete(save=True)
        super(Photo, self).delete(*args, **kwargs)
