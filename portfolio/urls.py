from django.conf.urls import patterns, url
from portfolio import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='portfolio_index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^account/$', views.account, name='account'),
    url(r'^upload/$', views.upload, name='upload'),
    url(r'^photos/$', views.photos, name='photos'),
    url(r'^photos/(?P<photo_id>\d+)$', views.photos, name='photos'),

    url(r'^photographers/$', views.photographers, name='photographers'),
    url(r'^photographers/(?P<photo_id>\d+)$', views.photographers, name='photographers'),

    url(r'^search/$', views.search_for, name='search_for'),
    url(r'^random/(?P<random_type>\w+)$', views.random_p, name='random_p'),

    url(r'^account/photos/(?P<photo_id>\d+)$', views.account_photos, name='account_photos'),
    url(r'^account/photos/$', views.account_photos, name='account_photos'),
    url(r'^account/photos/edit/(?P<photo_id>\d+)', views.account_photos_edit, name='account_photos_edit'),
    url(r'^account/photos/delete/(?P<photo_id>\d+)$', views.account_photos_delete, name='account_photos_delete'),
    url(r'^account/photos/delete/$', views.account_photos_delete, name='account_photos_delete'),
    url(r'^account/edit/$', views.account_edit, name='account_edit'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
)

