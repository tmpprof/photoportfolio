function PhotoEditor(img, canvas){
	this.img=img;
	this.canvas=canvas;
	this.context=canvas.getContext('2d');
}
PhotoEditor.prototype.resizeCanvas = function(width, height) {
	width = (typeof width === 'undefined') ? this.img.width : width;
	height = (typeof height === 'undefined') ? this.img.height : height;
	this.canvas.height=height;
	this.canvas.width = width;
	return this;
};
PhotoEditor.prototype.drawImage = function() {
	var args = [this.img].concat(Array.prototype.slice.call(arguments,0));
	this.context.drawImage.apply(this.context, args)
	return this;
};

function initPhotoEditor(){
	var i = document.getElementsByTagName('img')[0];
	var img = new Image();
	img.src=i.src;
	var canvas = document.getElementsByTagName('canvas')[0];

	var photoEditor = new PhotoEditor(img, canvas);
	photoEditor.resizeCanvas();
	photoEditor.context.drawImage(photoEditor.img, 0,0);

	return photoEditor;
}

